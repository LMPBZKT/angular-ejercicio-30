import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-div',
  templateUrl: './div.component.html',
  styleUrls: ['./div.component.css']
})
export class DivComponent implements OnInit {

  cambiar: number = 30
  text: string = '';
  textErr: string = 'Llegó al limite'
  constructor() { }

  ngOnInit(): void {

  }

  aumentar(){
    this.cambiar = this.cambiar + 5
    if (this.cambiar === 500){
      this.text = this.textErr
    }
  }

  disminuir(){
    this.cambiar = this.cambiar - 5;
    if(this.cambiar === 5){
      this.text = this.textErr
    }
  }
}
